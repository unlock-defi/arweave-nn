import csvParser from 'csv-parser';
import fs from 'fs';
import winston from 'winston';
import { createInterface } from 'readline';

/*********************
 * Command to Run the Json generation process
 * 
 * node .\generateJSONFiles.js ./assets/processed_airdrop_png_mp4.csv attributes-airdrop ./input/example_metadata_airdrop.json
 * 
 * The first argument is for a csv file which has the Arweave Transaction IDs for both Gifs/Pngs and MP4s
 * The second argument is for the folder that contains the attributes
 * The third argument is for the json template
 */

const APP_PATH = process.cwd();
const jsonDir = APP_PATH + "/json-updated";
let attrDir = APP_PATH + "/attributes";

const buildingDir = APP_PATH + "/buildings";
const buildingNamesFile = "Vetted_Name_List.csv";
//csv file name that has the Arweave TX IDs
let buildingTxId = "";
//file name for the Json template
let fileName = '';
const ARWEAVE_URL = 'https://arweave.net/';

if(process.argv[2] != null){    
    buildingTxId = process.argv[2];
    console.log(`Arweave TX IDs - ${buildingTxId}`);    
}else{
    console.log("need the input file with the Arweave Transaction IDs for the assets");
    process.exit(1);
}

if(process.argv[3] != null){    
    attrDir = APP_PATH + "/" + process.argv[3];
    console.log(`Attributes folder - ${attrDir}`);    
}else{
    console.log("attributes path need to be supplied");
    process.exit(1);
}

if(process.argv[4] != null){    
    fileName = process.argv[4];
    console.log(fileName);    
    //process.exit(1);
}else{
    console.log("json template is missing");
    process.exit(1);
}

//logger
let logger = winston.createLogger({
    level: 'debug',
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.printf(info => {
            return `${info.timestamp} ${info.level}: ${info.message}`;
        })
    ),
    transports: [new winston.transports.File({filename: `./logs/generate_json_test_with_name.log`})] 
})
///////

logger.log('info', 'starting to generate json file for each of the asset');
let data = fs.readFileSync(fileName); 
const json = JSON.parse(data);
logger.log('info', `template file for generating json ${fileName}`);
console.log(json.symbol);

//remove the first attribute as it is a placeholder
json.attributes.pop();
console.log(json.attributes);

let buildingNamesMap = loadBuildingNamesFile();

fs.createReadStream(buildingTxId).pipe(csvParser()).on('data', function(row){
    //const file_name = row.file_name_no_ext;
    const file_name = row.file_name;    
    const gif_id = row.png_id;
    const mp4_id = row.mp4_id;
    console.log("file name - " + file_name);

    const jsonObj = JSON.parse(JSON.stringify(json));

    if(buildingNamesMap != null)
        jsonObj.name = 'The ' + buildingNamesMap.get(file_name);
    jsonObj.image = getGifURL(gif_id);
    jsonObj.animation_url = getAnimationURL(mp4_id);
    jsonObj.properties.files[0].uri = getGifURL(gif_id);
    jsonObj.properties.files[1].uri = getAnimationURL(mp4_id);

    let attrFileName = `${attrDir}/${file_name}.txt`;
    if( fs.existsSync(attrFileName) ){
            

        addAttr(file_name, jsonObj);

        let writeStream = fs.createWriteStream(`${jsonDir}/${file_name}.json`);
        writeStream.write(JSON.stringify(jsonObj, null, 4));
        console.log("json file is saved");
    }else{
        console.log(`file does ${file_name} not exist`);
    }
    //addAttr(file_name);
})

function loadBuildingNamesFile(){
    let attrFileName = `${buildingDir}/${buildingNamesFile}`;
    if(attrFileName == null){
        logger.log('error', 'building names file not available ' + attrFileName);
        return;
    }
    let data = fs.readFileSync(attrFileName, 'utf8');
    const lines = data.split(/\r?\n/);
    const namesMap = new Map();
    lines.forEach((line) => {
        //console.log(line);
        let values = line.split(",");
        namesMap.set(values[0], values[1]);
    });
    console.log("loaded building names");
    return namesMap;
}

function addAttr(fileName, jsonObj){
    let attrFileName = `${attrDir}/${fileName}.txt`;
    if(attrFileName == null){
        logger.log('error', 'atrribute file not available ' + attrFileName);
        return;
    }
    let data = fs.readFileSync(attrFileName, 'utf8');

    const lines = data.split(/\r?\n/);
    lines.forEach((line) => {
        //console.log(line);
        if(line != ''){
            //split the string based on '=' or ':' to assign the attributes
            let values = line.split("=");
            if(values[0] != 'Name'){
                
                jsonObj.attributes.push({
                    trait_type: `${values[0]}`,
                    value: values[1].trim()
                });
            }else{
                jsonObj.name = values[1].trim();
                jsonObj.description = values[1].trim();
            }
        }
    });
    console.log("added atrributes to " + fileName);
    
}
                  
function addAttributes(fileName, jsonObj){
    let attrFileName = `${attrDir}/${fileName}.txt`;
    if(attrFileName == null){
        logger.log('error', 'atrribute file not available ' + attrFileName);
        return;
    }
    let attributeData = fs.readFileSync(attrFileName); 
    const attrFile = createInterface({
        input: fs.createReadStream(attrFileName),
        output: process.stdout,
        terminal: false
    });

    attrFile.on('line', (line) => {
        //console.log(line);
        if(line != null && line.indexOf(":") > 0){
            let values = line.split(":");
            //console.log(values);
            //console.log(`${values[0]} , ${values[1]}`);
            jsonObj.attributes.push({
                trait_type: `${values[0]}`,
                value: values[1]
            });
            
        }

    })
    console.log(jsonObj.attributes);
}

function getGifURL(arweaveAddress){
    let address = `${ARWEAVE_URL}${arweaveAddress}?ext=png`;
    return address;
}

function getAnimationURL(arweaveAddress){
    let animationURL = `${ARWEAVE_URL}${arweaveAddress}?ext=mp4`;
    return animationURL;
}
//JSON.
